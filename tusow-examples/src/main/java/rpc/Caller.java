package rpc;

import it.unibo.coordination.linda.logic.LogicMatch;
import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class Caller {

    /**
     *
     * @param args 0: process name, 1: + | - | * | / (all lowercase, no special symbols, letters first), 2: first operand, 3: second operand
     *
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LogicSpace space = RemoteLogicSpace.of();
        String me = args[0].toLowerCase();
        String fun = args[1].toLowerCase();
        int op1 = Integer.parseInt(args[2]);
        int op2 = Integer.parseInt(args[3]);
        Logger.getAnonymousLogger().info(me + " calling " + op1 + " " + fun + " " + op2);
        CompletableFuture<LogicTuple> call = space.write("call(fun(" + fun + ", " + op1 + ", " + op2 + "), who(" + me + "))");
        call.get();  // best practice to wait completion, as subsequent invocation could mix up
        Logger.getAnonymousLogger().info("Waiting result...");
        CompletableFuture<LogicMatch> res = space.take("return(res(Res), who(" + me + "))");
        Logger.getAnonymousLogger().info("Got result " + res.get().get("Res").get());
        System.exit(0);
    }

}
