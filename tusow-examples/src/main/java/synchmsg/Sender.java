package synchmsg;

import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class Sender {

    /**
     *
     * @param args 0: process name, 1: receiver name, 2: salutation (all lowercase, no special symbols, letters first)
     *
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LogicSpace space = RemoteLogicSpace.of();
        String me = args[0].toLowerCase();
        Logger.getAnonymousLogger().info(me + " ready to send"); // uppercase are variables in Prolog
        String who = args[1].toLowerCase();
        CompletableFuture<LogicTuple> ready = space.takeTuple("ready(who(" + who + "))");
        ready.get();  // let's wait indefinitely now, not a good idea in general...
        String salutation = args[2].toLowerCase();
        Logger.getAnonymousLogger().info(who + " is ready to receive, sending " + salutation);
        space.write("msg(to(" + who + "), from(" + me + "), content(" + salutation + "))")
                .get();  // let's wait indefinitely now, not a good idea in general...
        System.exit(0);
    }

}
