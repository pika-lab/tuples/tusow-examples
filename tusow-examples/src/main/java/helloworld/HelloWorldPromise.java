package helloworld;

import it.unibo.coordination.linda.logic.LogicMatch;
import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

public class HelloWorldPromise {

    public static void main(String[] args) {
        LogicSpace space = RemoteLogicSpace.of();
        CompletableFuture<LogicTuple> writeHello = space.write("hello(everybody)");   // asynch
        writeHello.thenRun(() -> {
            CompletableFuture<LogicMatch> readHello = space.read("hello(Who)");    // get match
            readHello.thenAccept(helloRes -> {
                Logger.getAnonymousLogger().info("Greetings to: " + helloRes.get("Who").get()); // get bound variable value
                System.exit(0);
            });
        });
    }

}
