package publishSubscribe;

import alice.tuprolog.Number;
import it.unibo.coordination.linda.logic.LogicMatch;
import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTemplate;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class Subscriber {

    /**
     *
     * @param args 0: process name (all lowercase, no special symbols, letters first)
     */
    public static void main(String[] args) {
        LogicSpace space = RemoteLogicSpace.of();
        String me = args[0].toLowerCase();
        Logger.getAnonymousLogger().info(me + " waiting for jobs...");
        for (int i = 0; i < 30; i++) {
            CompletableFuture<Collection<LogicMatch>> jobs = space.takeAll("job(id(Who, Idx), task(factorial, N))");
            jobs.thenAccept(list -> {
                try {
                    handleJobs(space, list);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private static void handleJobs(LogicSpace space, Collection<LogicMatch> list) throws ExecutionException, InterruptedException {
        if (!list.isEmpty()) {
            for (LogicMatch match : list) {
                int idx = match.get("Idx").get().castTo(Number.class).intValue();
                int n = match.get("N").get().castTo(Number.class).intValue();
                Logger.getAnonymousLogger().info("Got " + idx + " job: " + n);
                CompletableFuture<LogicTuple> res = space.write("result(id(" + match.get("Who").get() + ", " + idx + "), res(" + Subscriber.factorial(n) + "))");
                res.get();
            }
        } else {
            Logger.getAnonymousLogger().info("No jobs yet");
            Thread.sleep(2000);
        }
    }

    private static int factorial(int n) {
        if (n < 1) {
            return 1;
        } else {
            return n * Subscriber.factorial(n-1);
        }
    }

}
