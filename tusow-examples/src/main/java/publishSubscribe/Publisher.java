package publishSubscribe;

import it.unibo.coordination.linda.logic.LogicMatch;
import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class Publisher {

    /**
     *
     * @param args 0: process name (all lowercase, no special symbols, letters first), 1: factorial to ask for
     *
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LogicSpace space = RemoteLogicSpace.of();
        String me = args[0].toLowerCase();
        int n = Integer.parseInt(args[1]);
        Logger.getAnonymousLogger().info(me + " submitting jobs for " + n + " factorials from " + n + " to 0");
        for (int f = n; f > 0; f--) {
            CompletableFuture<LogicTuple> job = space.write("job(id(" + me + ", " + (n-f) + "), task(factorial, " + f + "))");
            job.get();
        }
        while (n > 0) {
            CompletableFuture<Collection<LogicMatch>> results = space.takeAll("result(id(" + me + ", Idx), res(F))");
            Collection<LogicMatch> list = results.get();
            n -= list.size();
            if (!list.isEmpty()) {
                for (LogicMatch match : list) {
                    Logger.getAnonymousLogger().info("Got " + match.get("Idx").get() + " result: " + match.get("F").get());
                }
            } else {
                Logger.getAnonymousLogger().info("No results yet");
            }
            Thread.sleep(5000);
        }
    }

}
