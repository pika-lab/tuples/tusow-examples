package synchmsg;

import it.unibo.coordination.linda.logic.LogicMatch;
import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class Receiver {

    /**
     *
     * @param args 0: process name, 1: sender name (all lowercase, no special symbols, letters first)
     *
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LogicSpace space = RemoteLogicSpace.of();
        String me = args[0].toLowerCase();
        Logger.getAnonymousLogger().info(me + " ready to receive");
        CompletableFuture<LogicTuple> ready = space.write("ready(who(" + me + "))");
        ready.get();    // best practice to wait completion, as subsequent invocation could mix up
        String who = args[1].toLowerCase();
        CompletableFuture<LogicMatch> msg = space.take("msg(to(" + me + "), from(" + who + "), content(Content))");    // keep variable Content in mind...
        LogicMatch msgRes = msg.get();
        Logger.getAnonymousLogger().info("Got msg '" + msgRes.get("Content").get() + "' from " + who);  // ...use it to access bound value
        System.exit(0);
    }

}
