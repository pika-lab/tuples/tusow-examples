package helloworld;

import it.unibo.coordination.linda.logic.LogicMatch;
import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class HelloWorld {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        LogicSpace space = RemoteLogicSpace.of();
        CompletableFuture<LogicTuple> writeHello = space.write("hello(everybody)");   // asynch
        writeHello.get(10, TimeUnit.SECONDS); // wait for completion
        if (writeHello.isDone()) {
            CompletableFuture<LogicMatch> readHello = space.read("hello(Who)");    // get match
            LogicMatch helloRes = readHello.get(10, TimeUnit.SECONDS);
            if (readHello.isDone()) {
                Logger.getAnonymousLogger().info("Greetings to: " + helloRes.get("Who").get()); // get bound variable value
                System.exit(0);
            }
        } else {
            Logger.getAnonymousLogger().info("Something went wrong");
        }
    }

}
