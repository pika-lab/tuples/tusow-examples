package mailbox;

import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class Sender {

    /**
     *
     * @param args 0: process name, 1: receiver name, 2: salutation (all lowercase, no special symbols, letters first)
     *
     * @throws ExecutionException
     * @throws InterruptedException
     * @throws TimeoutException
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LogicSpace space = RemoteLogicSpace.of();
        String me = args[0].toLowerCase();
        String who = args[1].toLowerCase();
        String content = args[2].toLowerCase();
        CompletableFuture<LogicTuple> mail = space.write("mail(to(" + who + "), content(" + content + "), from(" + me + "))");
        Logger.getAnonymousLogger().info("Sent mail: " + mail.get());
        System.exit(0);
    }

}
