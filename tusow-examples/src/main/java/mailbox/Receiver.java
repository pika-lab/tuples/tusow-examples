package mailbox;

import it.unibo.coordination.linda.logic.LogicMatch;
import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class Receiver {

    /**
     *
     * @param args 0: process name (all lowercase, no special symbols, letters first)
     *
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LogicSpace space = RemoteLogicSpace.of();
        String me = args[0].toLowerCase();
        for (int i = 0; i < 6; i++) { // 1 min
            CompletableFuture<LogicMatch> mail = space.tryTake("mail(to(" + me + "), content(Content), from(Who))");
            if (mail.get().getTuple().isPresent()) {
                Logger.getAnonymousLogger().info("Got mail from '" + mail.get().get("Who").get() + "': " + mail.get().get("Content").get());
                System.exit(0);
            } else {
                Logger.getAnonymousLogger().info("No mail yet");
                Thread.sleep(10000);    // 10 sec
            }
        }
        System.exit(0);
    }

}
