package rpc;

import it.unibo.coordination.linda.logic.LogicMatch;
import it.unibo.coordination.linda.logic.LogicSpace;
import it.unibo.coordination.linda.logic.LogicTuple;
import it.unibo.coordination.linda.logic.remote.RemoteLogicSpace;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class Callee {

    /**
     *
     * @param args 0: process name (all lowercase, no special symbols, letters first)
     *
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LogicSpace space = RemoteLogicSpace.of();
        String me = args[0].toLowerCase();
        for (int i = 0; i < 6; i++) {   // 1 min
            Logger.getAnonymousLogger().info(me + " waiting for calls...");
            CompletableFuture<LogicMatch> call = space.tryTake("call(fun(Fun, Op1, Op2), who(Who))");
            LogicMatch callRes = null;
            if (call.get().getTuple().isPresent()) {
                callRes = call.get();
                Logger.getAnonymousLogger().info("Call received: " + callRes.getTuple());
                double op1 = Double.parseDouble(callRes.get("Op1").get().toString());
                double op2 = Double.parseDouble(callRes.get("Op2").get().toString());
                double res;
                switch (callRes.get("Fun").get().toString()) {
                    case "sum":
                        res = op1 + op2;
                        break;
                    case "sub":
                        res = op1 - op2;
                        break;
                    case "mul":
                        res = op1 * op2;
                        break;
                    case "div":
                        res = op1 / op2;
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + call.get().get("Fun").toString());
                }
                String who = callRes.get("Who").get().toString();
                Logger.getAnonymousLogger().info("Returning " + res + " to " + who);
                CompletableFuture<LogicTuple> reply = space.write("return(res(" + res + "), who(" + who + "))");
                reply.get();   // best practice to wait completion, as subsequent invocation could mix up
            } else {
                Logger.getAnonymousLogger().info("No calls yet");
                Thread.sleep(10000);    // 10 sec
            }
        }
        System.exit(0);
    }

}
